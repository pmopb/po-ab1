#include <stdio.h>
#include <ilcplex/ilocplex.h>
#include <vector>
#include <utility>
#define MAX 50
using std::vector;
using std::pair;
int main() {

	IloEnv env;
	IloModel modelo(env, "Steiner");
	IloCplex cplex(modelo);

	vector<int> T(MAX); // vetor dos terminais
	vector<int> C(MAX);// vetor de custo
	pair<int, int> arestas[MAX];

	int v, e, t, v1, v2, w;
	// fun��es de io da entrada padr�o da lib do C s�o mais perfomaticas que as da stdlib(c++)
	// tratamento da entrada
	scanf_s("%d %d", &v, &e);
	for (auto i = 0; i < e; ++i) {
		scanf_s("%d %d %d", &v1, &v2, &w);
		--v1, --v2;
		arestas[i] = { v1,v2 };
		C[i] = w;
	}

	scanf_s("%d", &t);
	for (auto i = 0; i < t; ++i) {
		scanf_s("%d", &v1);
		--v1;
		T[v1] = 1;
	}

	/*Variaveis de decisao: AT arestas, VT vertices, da arvore*/
	IloBoolVarArray AT(env, e), VT(env, v);
	IloArray<IloNumVarArray> fluxo(env, e);

	for (int i = 0; i < e; ++i)
		fluxo[i] = IloNumVarArray(env, 2, 0, IloInfinity);

	/*Restri��es: todo terminal est� na �rvore, todo terminal tem pelo menos um vertice,
	nao terminais tem pelo menos duas arestas*/
	IloExpr totalTerminals(env);
	for (int i = 0; i < v; ++i)if (T[i])
		totalTerminals += VT[i];

	modelo.add(totalTerminals == t);
	for (int i = 0; i < v; ++i) {
		if (T[i]) {
			IloExpr total(env);
			for (int j = 0; j < e; ++j) {
				if (arestas[j].first == i || arestas[j].second == i) {
					total += AT[j];
				}
			}
			modelo.add(total >= 1);
		}
		else {
			IloExpr total(env);
			for (int j = 0; j < e; ++j) {
				if (arestas[j].first == i || arestas[j].second == i) 
					total += AT[j];
				
			}
			modelo.add(total >= 2 * VT[i]);
		}
	}
	IloExpr tvertices(env);
	IloExpr tarestas(env);
	for (int i = 0; i < e; ++i) 
		tarestas += AT[i];
	for (int i = 0; i < v; ++i) 
		tvertices += VT[i];
	modelo.add(tvertices == tarestas + 1);
	for (int i = 0; i < e; ++i) 
		modelo.add(fluxo[i][0] + fluxo[i][1] == (2 * AT[i]));
	

	IloExprArray somadofluxo(env, v);

	for (int i = 0; i < v; ++i)
		somadofluxo[i] = IloExpr(env);
	for (int i = 0; i < e; ++i) {
		somadofluxo[arestas[i].first] += fluxo[i][0];
		somadofluxo[arestas[i].second] += fluxo[i][1];
	}
	for (int i = 0; i < v; ++i) 
		modelo.add(somadofluxo[i] <= (2.0 - 2.0 / v));
	
	/*Funcao objetivo*/
	IloExpr custoFinal(env);
	for (int i = 0; i < e; ++i)
		custoFinal += AT[i] * C[i];
	modelo.add(IloMinimize(env, custoFinal));

	cplex.solve();
	printf("Custo minimo %lf\n", cplex.getObjValue());
	IloNumArray sol(env, e);
	cplex.getValues(sol, AT);
	for (int i = 0; i < e; ++i) 
		if (sol[i])
			printf("%d - %d\n", arestas[i].first + 1, arestas[i].second + 1);

	env.end();
	return 0;
}