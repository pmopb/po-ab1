#include <stdio.h>
#include <ilcplex/ilocplex.h>
#include <vector>
#include <algorithm>
#define MAX 50
using std::vector;

int main() {

    IloEnv env;
    IloModel modelo(env, "Biclique m�xima balanceada");
    IloCplex cplex(modelo);

	vector<vector<int>> V(MAX);
	vector<vector<int>> V2(MAX);

    int si, sj, aresta, k, v;
    
    scanf_s("%d %d %d", &si, &sj, &aresta);
    for(int i = 0; i < aresta; ++i){
        scanf_s("%d %d", &k, &v);
        --k,--v;
        V[k][v] = 1;
        V2[v][k] = 1;
    }

    /*Vari�veis de decis�o, g1 e g2 presentes na solu��o, respectivamente*/
    IloBoolVarArray v1(env, si), v2(env, sj); 

    /*Restri��es do balanceamento*/
	IloExpr iV1(env), iV2(env), maxV(env);
        for(int i = 0; i < si; ++i)
			iV1+= v1[i];
        for(int i = 0; i < sj; ++i)
			iV2+= v2[i];
        modelo.add(iV1 == iV2);
        // Da clique
        for(int i = 0; i < si; ++i)
            for(int j = 0; j < sj; ++j)
                if(!V[i][j])
                    modelo.add(v1[i]+v2[j] <= 1);
                
    /*O objetivo � maximizar a quantidade de v�rtices*/
	for(int i = 0; i < si; ++i)
		maxV+= v1[i];
	for(int i = 0; i < sj; ++i)
		maxV+= v2[i];
	modelo.add(IloMaximize(env, maxV));

    cplex.solve();
    printf("N Vertices %.0lf \n", cplex.getObjValue());
    IloNumArray g1(env), g2(env);
    cplex.getValues(g1, v1);
    cplex.getValues(g2, v2);
    printf("Vertices do G1:");
    for(int i = 0; i < si; ++i)
        if(g1[i])printf("%d ", i+1);
    
    printf("\nVertices do G2:");
    for(int i = 0; i < sj; ++i)
        if(g2[i])printf("%d ", i+1);

    return 0;
}