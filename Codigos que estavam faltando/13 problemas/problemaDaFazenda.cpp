#include <stdio.h>
#include <ilcplex/ilocplex.h>
#define qtFazendas 3
#define qtPlant 3
using namespace std;

// Informa��es do problema
// Data provided by the problem
double area[qtFazendas] = {400, 650, 350},
mxArea[qtFazendas] = {660, 880, 400},
agua[qtFazendas] = {1800, 2200, 950},
aguapProp[qtFazendas] = {5.5, 4, 3.5},
lucro[qtFazendas] = {5000, 4000, 1800};

int main() {
    
    IloEnv env;
    IloModel modelo(env, "Problema da Fazenda");
    IloCplex cplex(modelo);

    // Vari�veis de decis�o
    IloArray<IloNumVarArray> fazenda(env, qtFazendas);
    for (auto i = 0; i < qtFazendas; i++) {
        fazenda[i] = IloNumVarArray(env, qtFazendas);
        for (auto j = 0; j < qtFazendas; j++) 
            fazenda[i][j] = IloNumVar(env);
    }

    // Restri��es: Area por Plantio, agua por fazenda, area por fazenda, propor��o, igualdade
    for (auto i = 0; i < qtPlant; i++) {
        IloExpr rest(env);
        for (auto j = 0; j < qtFazendas; j++) 
            rest += fazenda[i][j];
        modelo.add(rest <= mxArea[i]);
    }

    for (auto i = 0; i < qtFazendas; i++) {
        IloExpr rest(env);
        for (auto j = 0; j < qtPlant; j++) 
            rest += fazenda[i][j] * aguapProp[i];
        
        modelo.add(rest <= agua[i]);
    }

    for (auto i = 0; i < qtFazendas; i++) {
        IloExpr rest(env);
        for (auto j = 0; j < qtPlant; j++) 
            rest += fazenda[i][j];
        modelo.add(rest <= area[i]);
    }

    IloExprArray restProp(env, qtFazendas);
    
    for (int i = 0; i < qtFazendas; i++) {
        IloExpr rest(env);
        for (int j = 0; j < qtPlant; j++) 
            rest += fazenda[i][j];
        restProp[i] = rest / area[i];
    }
    
    modelo.add(restProp[0] == restProp[1] == restProp[2]);

    // Func objetivo: maximizar o lucro
    IloExpr fnobj(env);
    for (int i = 0; i < qtPlant; i++) {
        IloExpr fnobj_part(env);
        for (int j = 0; j < qtFazendas; j++) 
            fnobj_part += fazenda[i][j];
        fnobj += (fnobj_part * lucro[i]);
    }

    modelo.add(IloMaximize(env, fnobj));

    cplex.solve();
	println("Func objetivo: %lf\n",cplex.getObjValue());
    for (int i = 0; i < qtFazendas; i++) 
        for (int j = 0; j < qtFazendas; j++) 
			printf("%d-%d %lf\n",i,j,cplex.getValue(fazenda[i][j]));

    cplex.end();
    modelo.end();
    env.end();
	return 0;
}
