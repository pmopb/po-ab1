#include <stdio.h>
#include <ilocplex.h>
using namespace std;

typedef IloArray<IloNumVarArray> matriz;

int main() {

    IloEnv env;
    IloModel problemaTransporte(env, "Problema do Transporte");
    IloCplex cplex(problemaTransporte);

    // Setando os valores das variaveis do problema

    double ofertas[3] = {120, 80, 80};
    double demandas[3] = {150, 70, 60};
    double custos[3][3] = {{8, 5, 6}, {15, 10, 12}, {3, 9, 10}};

    int i, j;

    // Variaveis de decisao
    matriz quantidadeFD(env, 3);
    for (i = 0; i < 3; i++) {
        quantidadeFD[i] = IloNumVarArray(env, 3);

        for (j = 0; j < 3; j++) {
            quantidadeFD[i][j] = IloNumVar(env);
        }
    }

    // Restricao da quantidade maxima dos produtos nas fabricas e nao negatividade
    for (i = 0; i < 3; i++) {
        IloExpr restricoes(env);

        for (j = 0; j < 3; j++) {
            restricoes += quantidadeFD[i][j];
            problemaTransporte.add(quantidadeFD[i][j] >= 0);
        }

        problemaTransporte.add(restricoes <= ofertas[i]);
    }

    // Restricao da quantidade maxima dos produtos nos depositos e nao negatividade
    for (j = 0; j < 3; j++) {
        IloExpr restricoes(env);

        for (i = 0; i < 3; i++) {
            restricoes += quantidadeFD[i][j];
            problemaTransporte.add(quantidadeFD[i][j] >= 0);
        }

        problemaTransporte.add(restricoes == demandas[j]);
    }

    // Funcao objetivo minimizar o custo do transporte
    IloExpr obj(env);
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            obj += quantidadeFD[i][j] * custos[i][j];
        }
    }
    problemaTransporte.add(IloMinimize(env, obj));

    // Resolvendo
    cplex.solve();
    cout << "Funcao Objetivo: " << cplex.getObjValue() << endl;

    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            cout << "x" << i << j << ": " << cplex.getValue(quantidadeFD[i][j]) << endl;
        }
    }

    cplex.end();
    problemaTransporte.end();
    env.end();

}
