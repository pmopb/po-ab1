#include <stdio.h>
#include <ilocplex.h>

using namespace std;

int main() {

    IloEnv env;
    IloModel problemaFacilidades(env, "Problema das facilidades");
    IloCplex cplex(problemaFacilidades);

    // Setando os depositos e clientes
    int depositos, clientes, i, j;
    scanf("%d %d", &depositos, &clientes);

    // Custos
    double custoInstalacao[depositos];
    double custoAtendimento[depositos][clientes];

    for (i = 0; i < depositos; i ++) {
        scanf("%lf", &custoInstalacao[i]);
    }

    for (i = 0; i < depositos; i++) {
        for (j = 0; j < clientes; j++) {
            scanf("%lf", &custoAtendimento[i][j]);
        }
    }

    // Variaveis de decisao Se o deposito foi construido
    IloBoolVarArray xi(env, depositos);
    // Variaveis de decisao Se o deposito atende o cliente
    IloArray<IloBoolVarArray> yi(env, depositos);

    for (i = 0; i < depositos; i++) {
        yi[i] = IloBoolVarArray(env, clientes);
    }

    // Restricao relacionado a construcao do deposito
    for (i = 0; i < depositos; i++) {
        IloExpr instalado(env);

        for (j = 0; j < clientes; j++) {
            instalado += yi[i][j];
        }

        problemaFacilidades.add(instalado <= xi[i] * clientes);
    }

    // Restricao relacionado ao atendimento do cliente
    for (j = 0; j < clientes; j++) {
        IloExpr atende(env);

        for (i = 0; i < depositos; i++) {
            atende += yi[i][j];
        }

        problemaFacilidades.add(atende == 1);
    }

    // Funcao objetivo minimizar o custo
    IloExpr custo(env);

    for (i = 0; i < depositos; i++) {
        custo += custoInstalacao[i] * xi[i];

        for (j = 0; j < clientes; j++) {
            custo += custoAtendimento[i][j] * yi[i][j];
        }
    }

    problemaFacilidades.add(IloMinimize(env, custo));

    // Resolvendo
    cplex.solve();
    cout << "Funcao Objetivo: " << cplex.getObjValue() << endl;
    IloNumArray solutionInstall(env, depositos);
    cplex.getValues(solutionInstall, xi);
    IloArray<IloNumArray> solutionServe(env, depositos);

    cout << "Depositos: " << endl;
    for (int i = 0; i < depositos; i++) {
        cout << "x" << i << " -> " << cplex.getValue(xi[i]) << endl;
    }

    cout << "Clientes: " << endl;
    for (int i = 0; i < depositos; i++) {
        for (int j = 0; j < clientes; j++) {
            cout << "x" << i << j << " -> " << cplex.getValue(yi[i][j]) << endl;
        }
    }
    cplex.end();
    problemaFacilidades.end();
    env.end();

}
