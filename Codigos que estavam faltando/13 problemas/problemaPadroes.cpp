#include <stdio.h>
#include <ilocplex.h>
#include <vector>
#include <cmath>

using namespace std;

struct impressao{
    int tampa;
    int folha;
    int tamanho;
    int tempo;
};

int main() {

    IloEnv env;
    IloModel problemaPadroes(env, "Problema dos padroes");
    IloCplex cplex(problemaPadroes);

    int n, i;
    scanf("%d", &n);

    vector<impressao>impressoes;

    for(i = 0; i < n; ++i){
        impressao p;
        scanf("%d %d %d %d", &p.tamanho, &p.folha, &p.tampa, &p.tempo);
        impressoes.push_back(p);
    }

    int quantidade[3], tempoLimite = 400;
    int custoFolha = 5, custoTampa = 3;

    int precoTinta = 50;

    quantidade[1] = 200;
    quantidade[2] = 50;

    IloIntVar y(env, 0, IloInfinity);
    IloIntVarArray x(env, n, 0, IloInfinity);

    // Restricao tempo
    IloExpr tempoTotal(env);

    for(i = 0; i < n; ++i){
        tempoTotal += x[i] * impressoes[i].tempo;
    }

    problemaPadroes.add(tempoTotal <= tempoLimite);

    IloExpr t1(env);
    IloExpr t2(env);

    // Restricao tamanho
    for(i = 0; i < n; ++i){
        if(impressoes[i].tamanho == 1)t1+=x[i];
        else t2+= x[i];
    }

    problemaPadroes.add(t1 <= quantidade[1]);
    problemaPadroes.add(t2 <= quantidade[2]);

    //Funcao objetivo maximizar o lucro
    IloExpr lucro;
    IloExpr totalFolhas(env);
    IloExpr totalTampa (env);
    IloExpr totalTampaFundo (env);

    for(i = 0; i < n; ++i){
        totalFolhas+= x[i] * impressoes[i].folha;
        totalTampa+= x[i] * impressoes[i].tampa;
    }

    problemaPadroes.add(y <= totalFolhas);
    problemaPadroes.add(y <= totalTampa / 2);

    lucro = y * precoTinta - custoFolha * (totalFolhas - y) - custoTampa * (totalTampa - (y*2));

    problemaPadroes.add(IloMaximize(env, lucro));
    //Resolvendo
    cplex.solve();

    printf("Maximum lucro = %lf\n", cplex.getObjValue());

    IloInt solucao = cplex.getValue(y);

    printf("%d tins\n", solucao);

    IloNumArray quantidades(env);

    cplex.getValues(quantidades, x);

    for(i = 0; i < n; ++i){
        printf("%.0lf of type %d\n", fabs(quantidades[i]), i+1);
    }

    env.end();

}
