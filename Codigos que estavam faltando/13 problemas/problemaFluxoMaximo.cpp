#include <stdio.h>
#include <ilocplex.h>

using namespace std;

int main() {

    IloEnv env;
    IloModel problemaFluxoMax(env, "Problema do fluxo maximo");
    IloCplex cplex(problemaFluxoMax);

    // Variaveis de decisao n�s
    IloNumVar x01(env), x02(env), x03(env), x14(env), x15(env), x24(env), x25(env), x26(env), x35(env), x47(env), x57(env), x67(env);

    // Restricao relacinada a capacidade
    problemaFluxoMax.add(x01 <= 3);
    problemaFluxoMax.add(x02 <= 2);
    problemaFluxoMax.add(x03 <= 2);
    problemaFluxoMax.add(x14 <= 5);
    problemaFluxoMax.add(x15 <= 1);
    problemaFluxoMax.add(x24 <= 1);
    problemaFluxoMax.add(x25 <= 3);
    problemaFluxoMax.add(x26 <= 1);
    problemaFluxoMax.add(x35 <= 1);
    problemaFluxoMax.add(x47 <= 4);
    problemaFluxoMax.add(x57 <= 2);
    problemaFluxoMax.add(x67 <= 4);

    // Restricao relacionada a conservacao de fluxo
    problemaFluxoMax.add(x01 == (x14 + x15));
    problemaFluxoMax.add(x02 == (x24 + x25 + x26));
    problemaFluxoMax.add(x03 == x35);
    problemaFluxoMax.add((x14 + x24) == x47);
    problemaFluxoMax.add((x15 + x25 + x35) == x57);
    problemaFluxoMax.add(x26 == x67);

    // Funcao objetivo maximizar o fluxo
    problemaFluxoMax.add(IloMaximize(env, x01 + x02 + x03));

    // Resolvendo
    cplex.solve();
    cout << "Funcao Objetivo: " << cplex.getObjValue() << endl;
    cout << "x01: " << cplex.getValue(x01) << endl;
    cout << "x02: " << cplex.getValue(x02) << endl;
    cout << "x03: " << cplex.getValue(x03) << endl;
    cout << "x14: " << cplex.getValue(x14) << endl;
    cout << "x15: " << cplex.getValue(x15) << endl;
    cout << "x24: " << cplex.getValue(x24) << endl;
    cout << "x25: " << cplex.getValue(x25) << endl;
    cout << "x26: " << cplex.getValue(x26) << endl;
    cout << "x35: " << cplex.getValue(x35) << endl;
    cout << "x47: " << cplex.getValue(x47) << endl;
    cout << "x57: " << cplex.getValue(x57) << endl;
    cout << "x67: " << cplex.getValue(x67) << endl;

    cplex.end();
    problemaFluxoMax.end();
    env.end();

}
