#include <stdlib.h>
#include <ilocplex.h>
#include <math.h>
#include <set>

using namespace std;

typedef set<int> listaAdj;

int main() {

    IloEnv env;
    IloModel problemaCobertura(env, "Problema da Cobertura");
    IloCplex cplex(problemaCobertura);

    // Setando o tamanho do grafo n x m

    int n, m, i, j;

    scanf("%d %d", &n, &m);

    listaAdj grafo[n];

    int custos[n];

    // Gerando os custos
    for (i = 0; i < n; i++){
        custos[i] = rand() % 100;
    }

    // Construindo o grafo
    for (i = 0; i < m; i++) {
        int u, v;
        u = rand() % n;
        v = rand() % n;

        if (u != v) {
            grafo[u].insert(v);
            grafo[v].insert(u);
        }
    }

    // Variavel de decisao se foi construida ou nao
    IloBoolVarArray x(env, n);

    // Restricao cada bairro ter uma escola
    for (i = 0; i < n; i++) {
        IloExpr escola(env);
        escola += x[i];

        for (j : grafo[i]) {
            escola += x[j];
        }

        problemaCobertura.add(escola >= 1);
    }

    // Funcao objetivo minimizar o custo
    IloExpr custoFinal(env);
    for (i = 0; i < n; i++) {
        custoFinal += x[i] * custos[i];
    }
    problemaCobertura.add(IloMinimize(env, custoFinal));

    // Resolvendo
    cplex.solve();
    cout << "Funcao Objetivo: " << cplex.getObjValue() << endl;

    for (i = 0; i < n; i++) {
        cout << "x" << i << " -> " << cplex.getValue(x[i]) << endl;
    }

    // End
    cplex.end();
    problemaCobertura.end();
    env.end();

}
