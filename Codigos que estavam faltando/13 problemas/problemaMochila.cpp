#include <stdio.h>
#include <ilocplex.h>

using namespace std;

int main() {

    IloEnv env;
    IloModel problemaMochila(env, "Problema da Mochila");
    IloCplex cplex(problemaMochila);

    // Setando quantidade de itens e o peso da mochila
    int n, i;
    double peso;

    scanf("%d %lf", &n, &peso);

    // De cada item
    double pesos[n], valores[n];

    for (i = 0; i < n; i ++) {
        scanf("%lf %lf", &pesos[i], &valores[i]);
    }

    // Variavel de decisao se um item esta na mochila
    IloBoolVarArray contem(env, n);

    for (i = 0; i < n; i++) {
        contem[i] = IloBoolVar(env);
    }

    // Restricao do limite que a mochila pode suportar
    IloExpr total(env);
    for (i = 0; i < n; i++) {
        total += pesos[i] * contem[i];
    }
    problemaMochila.add(total <= peso);

    // Funcao objetivo maximizar os valores dos itens
    IloExpr obj(env);
    for (i = 0; i < n; i++) {
        obj += valores[i] * contem[i];
    }
    problemaMochila.add(IloMaximize(env, obj));

    // Resolvendo
    cplex.solve();
    cout << "Funcao Objetivo: " << cplex.getObjValue() << endl;

    for (i = 0; i < n; i++) {
        cout << "x" << i << " -> " << cplex.getValue(contem[i]) << endl;
    }
    cplex.end();
    problemaMochila.end();
    env.end();

}
