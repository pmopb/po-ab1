#include <stdio.h>
#include <set>
#include <math.h>
#include <ilocplex.h>

using namespace std;

typedef set<int> listaAdj;

int main() {

    IloEnv env;
    IloModel problemaCliqueMax(env, "Problema Clique Maximo");
    IloCplex cplex(problemaCliqueMax);

    int n, m, i, j;

    // Tamanho do grafo
    scanf("%d %d", &n, &m);

    listaAdj grafo[n];
    listaAdj subGrafo[n];

    // Construindo o grafo
    for (i = 0; i < m; i++) {
        int u, v;
        u = rand() % n;
        v = rand() % n;

        if (u != v) {
            grafo[u].insert(v);
            grafo[v].insert(u);
        }
    }

    for (i = 0; i < n; i++) {
        for (j = i + 1; j < n; j++) {
            if (!grafo[i].count(j)) {
                subGrafo[i].insert(j);
                subGrafo[j].insert(i);
            }
        }
    }

    // Variaveis de decisao se pertence ao clique maximo
    IloBoolVarArray x(env, n);

    // Restricao se existe um clique
    for (i = 0; i < n; i++) {
        for (j: subGrafo[i]) {
            problemaCliqueMax.add(x[i] + x[j] <= 1);
        }
    }

    // Funcao objetivo maximizar a quantidade de nos no clique
    IloExpr total(env);

    for (i = 0; i < n; i++) {
        total += x[i];
    }
    problemaCliqueMax.add(IloMaximize(env, total));

    // Resolvendo
    cplex.solve();
    cout << "Funcao Objetivo: " << cplex.getObjValue() << endl;

    for (i = 0; i < n; i++) {
        cout << "x" << i << " -> " << cplex.getValue(x[i]) << endl;
    }
    cplex.end();
    problemaCliqueMax.end();
    env.end();
}
