#include <stdio.h>
#include <ilocplex.h>

using namespace std;

typedef IloArray<IloBoolVarArray> matriz;

int main() {

    IloEnv env;
    IloModel problemaFrequencias(env, "Problema das Frequencias");

    IloCplex cplex(problemaFrequencias);

    int antenas, freq, i, j, k;
    antenas = 10;
    freq = 100;

    // Variaveis de decisao
    IloBoolVarArray frequencias(env, freq);

    matriz freq_ij(env, antenas);

    for (i = 0; i < antenas; i++) {
        freq_ij[i] = IloBoolVarArray(env, freq);

        for (j = 0; j < freq; j++) {
            freq_ij[i][j] = IloBoolVar(env);
        }
    }

    // Restricao relacionada a atribuicao
    for (i = 0; i < antenas; i++) {

        IloExpr total(env);

        for (j = 0; j < freq; j++) {
            total += freq_ij[i][j];
        }

        problemaFrequencias.add(total == 1);
    }

    for (i = 0; i < antenas; i++) {
        for (j = i + 1; j < antenas; j++) {
            for (k = 0; k < freq; k++) {

                problemaFrequencias.add(freq_ij[i][k] + freq_ij[j][k] <= 1);

            }
        }
    }

    // Funcao objetivo minimizar o numero de frequencias utilizadas
    IloExpr obj(env);
    for (j = 0; j < freq; j++) {
        obj += frequencias[j];
    }

    problemaFrequencias.add(IloMinimize(env, obj));

    // Resolvendo
    cplex.solve();
    cout << "Funcao objetivo: " << cplex.getObjValue() << endl;

    cplex.end();
    problemaFrequencias.end();
    env.end();
}
