#include <stdio.h>
#include <ilcplex/ilocplex.h>
using namespace std;

// Informações (constantes) do problema
#define precoCarne 4
#define precoCereal 1
#define amgsPreco 20.0
#define rePreco 30.0

double qtdAmgs[2] = {5, 1};
double qtdRe[2] = {2, 4};
double qtd[2] = {30000, 10000};

int main() {
    
    IloEnv env;
    IloModel model(env, "Problema da racao");
    IloCplex cplex(model);

    // Variaveis de decisao: qtd de amgs e re
    IloNumVar AMGS(env), RE(env);

    // Restricoes: p quantidade de carne e cereais
    model.add(AMGS >= 0 && RE >= 0);
    for (auto i = 0; i < 2; i++) 
        model.add(qtdAmgs[i] * AMGS + qtdRe[i] * RE <= qtd[i]);

    // Func objetivo: maximizar lucro
    model.add(IloMaximize(env, (amgsPreco - qtdAmgs[0] * precoCereal - 
        qtdAmgs[1] * precoCarne) * AMGS + (re_price - qtdRe[0] * precoCereal - 
        qtdRe[1] * precoCarne) * RE));

    cplex.solve();
	printf("func objetivo: %lf\n
	amgs: %lf\n
	re %lf\n", cplex.getObjValue(), cplex.getValue(AMGS), cplex.getValue(RE));

    cplex.end();
    model.end();
    env.end();
	return 0;
}
