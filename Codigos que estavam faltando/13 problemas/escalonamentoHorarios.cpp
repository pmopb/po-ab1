#include <stdio.h>
#include <ilocplex.h>

using namespace std;

int main() {

    IloEnv env;
    IloModel escalonamentoHorarios(env, "Problema do escalonamento de horarios");
    IloCplex cplex(escalonamentoHorarios);

    int semana, diasTrab, i, j, quantidade;

    semana = 7;
    diasTrab = 5;
    quantidade = 20;

    // Gerando demandas
    int demanda[semana];

    for (i = 0; i < quantidade; i++) {
        demanda[i] = rand() % 10 + 1;
    }

    // Variaveis de decisao numero de enfermeiras no dia
    IloNumVarArray x(env, semana);

    for (i = 0; i < semana; i++) {
        x[i] = IloNumVar(env);
    }

    // Restricao relacionada a demanda e nao negatividade
    for (i = 0; i < semana; i++) {
        IloExpr quantEnfermeiras(env);
        escalonamentoHorarios.add(x[i] >= 0);

        for (j = 0; j < diasTrab; j++) {
            quantEnfermeiras += x[(i + j) % semana];
            escalonamentoHorarios.add(quantEnfermeiras >= demanda[(i + diasTrab - 1) % semana]);
        }
    }

    // Funcao objetivo minimizar a quantidade de enfermeiras
    IloExpr obj(env);

    for (i = 0; i < semana; i++) {
        obj += x[i];
    }

    escalonamentoHorarios.add(IloMinimize(env, obj));

    // Resolvendo
    cplex.solve();

    cout << "Funcao Objetivo: " << cplex.getObjValue() << endl;

    for (i = 0; i < semana; i++) {
        cout << "x" << i << " -> " << cplex.getValue(x[i]) << endl;
    }

    cplex.end();
    escalonamentoHorarios.end();
    env.end();

}
