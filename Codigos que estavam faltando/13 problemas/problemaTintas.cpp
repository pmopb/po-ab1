#include <stdio.h>
#include <ilocplex.h>

using namespace std;

int main()
{
    IloEnv env;

    IloModel problemaTintas(env, "Problema das tintas");

    IloCplex cplex(problemaTintas);

    // Setando os valores das variaveis do problema
    double solucoes[4] = {0.3, 0.6, 0.7, 0.4};
    double minimoComposicao[4] = {0.25, 0.5, 0.2, 0.5};


    double custoSolA, custoSolB, custoSec, custoCor, litrosSr, litrosSn;

    custoSolA = 1.5;
    custoSolB = 1.0;

    custoSec = 4.0;
    custoCor = 6.0;

    litrosSr = 1000;
    litrosSn = 250;

    // Variaveis de decisao
    IloNumVarArray tintaSr(env, 4);
    IloNumVarArray tintaSn(env, 4);

    int i;

    for (i = 0; i < 4; i++) {
        tintaSr[i] = IloNumVar(env);
        tintaSn[i] = IloNumVar(env);
    }

    // Restricao relacionada a nao negatividade
    for (i = 0; i < 4; i++) {
        problemaTintas.add(tintaSr[i] >= 0);
        problemaTintas.add(tintaSn[i] >= 0);
    }

    // Restricao relacionada a producao
    problemaTintas.add(tintaSr[0] + tintaSr[1] + tintaSr[2] + tintaSr[3] == litrosSr);

    problemaTintas.add(tintaSn[0] + tintaSn[1] + tintaSn[2] + tintaSn[3] == litrosSn);

    // Restricao relacionada a proporcao
    problemaTintas.add(((tintaSr[2] + tintaSr[0] * solucoes[0] + tintaSr[1] * solucoes[2]) / 1000) >= minimoComposicao[0]);

    problemaTintas.add(((tintaSr[3] + tintaSr[0] * solucoes[2] + tintaSr[1] * solucoes[3]) / 1000) >= minimoComposicao[1]);

    problemaTintas.add(((tintaSn[2] + tintaSn[0] * solucoes[0] + tintaSn[1] * solucoes[2]) / 1000) >= minimoComposicao[0]);

    problemaTintas.add(((tintaSn[3] + tintaSn[0] * solucoes[2] + tintaSn[1] * solucoes[3]) / 1000) >= minimoComposicao[1]);

    // Funcao objetivo minimizar o custo de producao de tintas SR e SN
    problemaTintas.add(IloMinimize(env, (tintaSr[0] * custoSolA + tintaSr[1] * custoSolB + tintaSr[2] * custoSec + tintaSr[3] * custoCor) + (tintaSn[0] * custoSolA + tintaSn[1] * custoSolB + tintaSn[2] * custoSec + tintaSn[3] * custoCor)));

    // Resolvendo
    cplex.solve();
    cout << "Funcao Objetivo: " << cplex.getObjValue() << endl;

    cout << "Tinta SR: " << endl;
    for (i = 0; i < 4; i++) {
        cout << "x" << i << ": " << cplex.getValue(tintaSr[i]) << endl;
    }

    cout << "Tinta SN: " << endl;
    for (i = 0; i < 4; i++) {
        cout << "x" << i << ": " << cplex.getValue(tintaSn[i]) << endl;
    }

    cplex.end();
    problemaTintas.end();
    env.end();

}
