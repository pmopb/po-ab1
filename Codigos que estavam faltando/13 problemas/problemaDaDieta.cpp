#include <stdio.h>
#include <ilcplex/ilocplex.h>
using namespace std;

// Informa��es do problema
const int n = 6, m = 2;
double info[m][n] = {{1, 0, 2, 2, 1, 2}, {0, 1, 3, 1, 3, 2}},
preco[n] = {35, 30, 60, 50, 27, 22},
qtMin[2] = {9, 19};

int main() {
    
    IloEnv env;
    IloModel modelo(env, "Problema da Dieta");
    IloCplex cplex(modelo);

    // Vari�veis de  decis�o
    IloNumVarArray vDecisao(env, n);
    for (auto i = 0; i < n; i++)
        vDecisao[i] = IloNumVar(env);

    // Restri��es
    for (auto i = 0; i < m; i++) {
        IloExpr rest(env);
        for (auto j = 0; j < n; j++) {
            rest += info[i][j] * vDecisao[i];
            modelo.add(vDecisao[i] >= 0);
        }
        modelo.add(rest >= qtMin[i]);
    }

    // Fun��o objetivo
    IloExpr fnobj(env);
    for (int i = 0; i < n; i++) {
        fnobj += preco[i] * vDecisao[i];
    }
    modelo.add(IloMinimize(env, fnobj));

    // Resolver
    cplex.solve();
	printf("Func objetivo :%lf\n", cplex.getObjValue());
    for (int i = 0; i < n; i++)
		printf("x %d: %lf\n",cplex.getValue(vDecisao[i]));

    cplex.end();
    modelo.end();
    env.end();
	return 0;
}
