#include <stdio.h>
#include <ilcplex/ilocplex.h>
#include <vector>
#include <utility>
constexpr auto MAX = 510;
using std::vector;
using std::pair;
using std::make_pair;

int main() {

	IloEnv env;
	IloModel modelo(env, "Coloracao de aresta c/ custo minimo");
	IloCplex cplex(modelo);
	int v, e, v1, v2;
	scanf_s("%d %d", &v, &e);
	vector< pair<int, int> > arestas;
	vector<int> adja[MAX];
	/*Guardamos as adjacencias entre vertices*/
	for (int i = 0; i < e; ++i) {
		scanf_s("%d %d", &v1, &v2);
		v1--; v2--;
		adja[v1].push_back(i);
		adja[v2].push_back(i);
		arestas.push_back(make_pair(v1, v2));
	}
	IloArray<IloBoolVarArray> cores(env, e);

	/*Variaveis de decisao*/
	for (int i = 0; i < e; ++i)cores[i] = IloBoolVarArray(env, e);
	/* Restricoes:
		Vertices devem ter apenas uma cor
		Adjaacencias nao podem ter a mesma cor
	*/
	for (int i = 0; i < e; ++i) {
		IloExpr tcor(env);
		for (int j = 0; j < v; ++j)
			tcor += cores[i][j];
		modelo.add(tcor == 1);
	}
	for (int i = 0; i < v; ++i)
		for (int v_ : adja[i]) 
			for (int _v : adja[i]) 
				if (v_ != _v) 
					for (int c = 0; c < e; ++c) 
						modelo.add(cores[v_][c] + cores[_v][c] <= 1);
				
	IloExpr C(env); 
	/*C e o custo de todas arestas, cor(i) custa i e i>0*/
	for (int i = 0; i < e; ++i) 
		for (int j = 0; j < e; ++j) 
			C += cores[i][j] * (j + 1);
		
	
	modelo.add(IloMinimize(env, C));
	cplex.solve();
	IloNumArray sol(env, e);
	printf("Fun objetivo = %lf\n", cplex.getObjValue());
	for (int i = 0; i < e; ++i) {
		cplex.getValues(sol, cores[i]);
		for (int j = 0; j < e; ++j) 
			if (sol[j]) 
				printf("vertice %d  = cor %d\n", i + 1, j + 1);
	}
	env.end();
	return 0;
}